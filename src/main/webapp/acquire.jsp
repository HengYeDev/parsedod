<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Defense Contract Data Analysis</title>
    <!-- Favicon-->
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="styles.css" rel="stylesheet" />
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
</head>
<body>
<div class="d-flex" id="wrapper">
    <!-- Page content wrapper-->
    <div id="page-content-wrapper">
        <!-- Page content-->
        <div class="container-fluid w-50 mt-5" id="mc">
            <h1 class="mt-4">Acquire and Parse Data</h1>
            <hr>
            <p class="pb-3">Acquire contract data from Defense Contract Notices using our intelligent AI text parser.</p>

            <p class="pb-3">Date range: <input type="text" id="daterange" value="04/01/2024 - 04/20/2024" /></p>
            <button type="button" class="btn btn-primary" id="continue">Continue</button>
            <hr>
            <a href="portal.jsp">Back to home</a>
        </div>
        <div class="container-fluid w-50 mt-5 d-none" id="lc">
            <h1 class="mt-4">Processing...</h1>
            <p>Please be patient, this may take a few minutes.</p>
        </div>

        <div class="container-fluid w-50 mt-5 d-none" id="dc">
            <h1 class="mt-4">Processing Completed</h1>
            <p>Data processing has completed. Please return to Home to view detailed data.</p>
            <a href="portal.jsp" class="btn btn-success">Go to home</a>

        </div>
    </div>
</div>
<!-- Bootstrap core JS-->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
<!-- Core theme JS-->
<script>
    $("#daterange").daterangepicker();
    $("#continue").click(function() {
        $('#lc').removeClass("d-none");
        $('#mc').addClass("d-none");
        $.post("/load", { "date": $('#daterange').val()} , function() {
            $('#lc').addClass("d-none");
            $('#dc').removeClass("d-none");
        });
    });
</script>
</body>
</html>