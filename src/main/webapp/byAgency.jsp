<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Data by Agency</title>
    <!-- Favicon-->
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="styles.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/1.13.6/css/jquery.dataTables.min.css" rel="stylesheet">
</head>
<body>
<div class="d-flex" id="wrapper">
    <!-- Sidebar-->
    <div class="border-end bg-white" id="sidebar-wrapper">
        <div class="sidebar-heading border-bottom bg-light"><a href="/portal.jsp"><img src="logo.png" style="width: 200px"></a></div>
        <div class="list-group list-group-flush">
            <a class="list-group-item list-group-item-action list-group-item-light p-3" href="acquire.jsp">Acquire Data</a>
            <a class="list-group-item list-group-item-action list-group-item-light p-3 " href="byCompany.jsp">By Company</a>
            <a class="list-group-item list-group-item-action list-group-item-light p-3 bg-primary text-white
" href="byAgency.jsp">By Agency</a>
            <a class="list-group-item list-group-item-action list-group-item-light p-3" href="byDate.jsp">By Date</a>

            <a class="list-group-item list-group-item-action list-group-item-light p-3" href="allData.jsp">All Data</a>
        </div>
    </div>
    <!-- Page content wrapper-->
    <div id="page-content-wrapper" class="h-100 w-100">
        <!-- Top navigation-->
        <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom p-3">
            <div class="container-fluid">
                <h1 class="h3">Data by Agency</h1>
            </div>
        </nav>
        <!-- Page content-->
        <div class="container-fluid h-100 w-100 mt-3 pb-5">
            <p><input type="text" class="form-control w-75 d-inline-block" placeholder="Search by federal agency name keyword or regular expression" id="search">
                <button type="button" class="btn btn-primary d-inline-block" id="go">Search</button></p>
            <hr>
            <p id="status">All contracts with federal agency name including the submitted text will be loaded.</p>

            <table id="tbl" class="table table-striped">
                <thead>
                <tr>
                    <th>Contract Number</th>
                    <th>Date Awarded</th>
                    <th>Expected Completion</th>
                    <th>Company Name</th>
                    <th>Federal Agency</th>
                    <th>Amount</th>
                    <th>Location</th>
                    <th>Type</th>
                </tr>
                </thead>
                <tbody id="body">

                </tbody>
            </table>
            <h4 class="d-none insights">Insights</h4>
            <canvas id="companies" style="max-width: 500px; max-height: 500px"></canvas>
            <br>
            <br>
            <h5 class="d-none insights">Locations of contracted companies</h5>
            <div id="map" style="height:750px; width: 800px"></div>


        </div>
    </div>
</div>
<!-- Bootstrap core JS-->
<script async
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBvDrrN1UnlblhVByHd4bJsBrDbejCR4wc&loading=async&callback=initMap">
</script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script src='https://cdn.datatables.net/1.13.5/js/jquery.dataTables.min.js'></script>
<script src='https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js'></script>
<script src='https://cdn.datatables.net/1.13.5/js/dataTables.bootstrap5.min.js'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/4.4.1/chart.umd.min.js" integrity="sha512-CQBWl4fJHWbryGE+Pc7UAxWMUMNMWzWxF4SQo9CgkJIN1kx6djDQZjh3Y8SZ1d+6I+1zze6Z7kHXO7q3UyZAWw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script><script>
    $('#tbl').hide();
    var agenciesChart;
    var dataTable;
    var geocoder;

    var map;
    var markers = [];

    function initMap() {
        var latlng = new google.maps.LatLng(37.09024, -95.712891);
        var myOptions = {
            zoom: 4,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        map = new google.maps.Map(document.getElementById("map"), myOptions);
        geocoder = new google.maps.Geocoder();
    }

    function codeAddress(address, popup) {
        try {
            geocoder.geocode({'address': address}, function (results, status) {
                if (status === 'OK') {
                    var marker = new google.maps.Marker({
                        map: map,
                        position: results[0].geometry.location
                    });


                    marker.addListener("click", function () {
                        const infoWindow = new google.maps.InfoWindow({
                            content: popup,
                            ariaLabel: popup
                        });

                        infoWindow.open({
                            anchor: marker,
                            map,
                        })
                    });
                    markers.push(marker);
                }
            });
        } catch(e) {
            console.log(e);
        }
    }

    function clearMarkers() {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(null);
        }
    }

    $('#map').hide();

    $('#go').click(function() {
        $('#tbl').hide();
        clearMarkers();

        if (dataTable)
            dataTable.destroy();

        $('#status').text("Loading results...this may take awhile!");
        $.get("/by-agency?term=" + $('#search').val(), function(data) {
            var arr = data.records;

            $('.insights').removeClass("d-none");
            $('#body').html("");
            $('#status').text(arr.length + " contracts found.");

            var companies = [];
            var companyAmounts = [];

            for (var i = 0; i < arr.length; i++) {
                $('#body').append("<tr><td>" + arr[i].contractNumber + "</td><td>" + arr[i].awarded + "</td><td>" + arr[i].date + "</td><td>" + arr[i].companyName + "</td><td>" + arr[i].agency + "</td><td>" + arr[i].amount + "</td><td>" + arr[i].location + "</td><td>" + arr[i].type + "</td></tr>");

                if (companies.includes(arr[i].companyName)) {
                    companyAmounts[companies.indexOf(arr[i].companyName)]++;
                } else {
                    companies.push(arr[i].companyName);
                    companyAmounts[companies.length - 1] = 1;
                }

                codeAddress(arr[i].location, arr[i].companyName);
            }


            dataTable = $('#tbl').DataTable({
                paging: true,
                searching: true,
                ordering: true,
            });

            if (agenciesChart) agenciesChart.destroy();

            agenciesChart = new Chart("companies", {
                type: "pie",

                data: {
                    labels: companies,
                    datasets: [
                        {
                            data: companyAmounts
                        }
                    ],
                },
                options: {
                    plugins: {
                        title: {
                            display: true,
                            text: "Companies contracted by this federal agency"
                        }
                    }
                }
            });


            $('#map').show();



            $('#tbl').show();

        });
    });
</script>
</body>
</html>