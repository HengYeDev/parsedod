<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Data by Company</title>
    <!-- Favicon-->
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="styles.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/1.13.6/css/jquery.dataTables.min.css" rel="stylesheet">
</head>
<body>
<div class="d-flex" id="wrapper">
    <!-- Sidebar-->
    <div class="border-end bg-white" id="sidebar-wrapper">
        <div class="sidebar-heading border-bottom bg-light"><a href="/portal.jsp"><img src="logo.png" style="width: 200px"></a></div>
        <div class="list-group list-group-flush">
            <a class="list-group-item list-group-item-action list-group-item-light p-3" href="acquire.jsp">Acquire Data</a>
            <a class="list-group-item list-group-item-action list-group-item-light p-3 bg-primary text-white
" href="byCompany.jsp">By Company</a>
            <a class="list-group-item list-group-item-action list-group-item-light p-3" href="byAgency.jsp">By Agency</a>
            <a class="list-group-item list-group-item-action list-group-item-light p-3" href="byDate.jsp">By Date</a>
            <a class="list-group-item list-group-item-action list-group-item-light p-3" href="allData.jsp">All Data</a>
        </div>
    </div>
    <!-- Page content wrapper-->
    <div id="page-content-wrapper" class="h-100 w-100">
        <!-- Top navigation-->
        <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom p-3">
            <div class="container-fluid">
                <h1 class="h3">Data by Company</h1>
            </div>
        </nav>
        <!-- Page content-->
        <div class="container-fluid h-100 w-100 mt-3">
            <p><input type="text" class="form-control w-75 d-inline-block" placeholder="Search by company name keyword or regular expression" id="search">
                <button type="button" class="btn btn-primary d-inline-block" id="go">Search</button></p>
            <hr>
            <p id="status">All contracts with company name including the submitted text will be loaded.</p>

            <table id="tbl" class="table table-striped">
                <thead>
                    <tr>
                        <th>Contract Number</th>
                        <th>Date Awarded</th>
                        <th>Expected Completion</th>
                        <th>Company Name</th>
                        <th>Federal Agency</th>
                        <th>Amount</th>
                        <th>Location</th>
                        <th>Type</th>
                    </tr>
                </thead>
                <tbody id="body">

                </tbody>
            </table>
            <h4 class="d-none insights">Insights</h4>
            <div class="row">
                <div class="col">
                    <canvas id="agencies" style="max-width: 500px; max-height: 500px"></canvas>
                </div>
                <div class="col">
                    <canvas id="types" style="max-width: 500px; max-height: 500px"></canvas>

                </div>
            </div>
            <br>
            <br>
            <canvas id="amounts" style="max-width: 1000px; max-height: 500px"></canvas>
            <br>
            <br>

        </div>
    </div>
</div>
<!-- Bootstrap core JS-->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script src='https://cdn.datatables.net/1.13.5/js/jquery.dataTables.min.js'></script>
<script src='https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js'></script>
<script src='https://cdn.datatables.net/1.13.5/js/dataTables.bootstrap5.min.js'></script>
<script src="https://cdn.jsdelivr.net/npm/moment"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/4.4.1/chart.umd.min.js" integrity="sha512-CQBWl4fJHWbryGE+Pc7UAxWMUMNMWzWxF4SQo9CgkJIN1kx6djDQZjh3Y8SZ1d+6I+1zze6Z7kHXO7q3UyZAWw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdn.jsdelivr.net/npm/chartjs-adapter-moment"></script>

<script>
    $('#tbl').hide();
    var agenciesChart;
    var amountChart;
    var dataTable;
    var typesChart;

    Chart.defaults.elements.point.borderWidth = 8;


    $('#go').click(function() {
        $('#tbl').hide();

        if (dataTable)
            dataTable.destroy();

        $('#status').text("Loading results...this may take awhile!");
        $.get("/by-company?term=" + $('#search').val(), function(data) {
            $('.insights').removeClass("d-none");
            var arr = data.records;

            $('#body').html("");
            $('#status').text(arr.length + " contracts found.");

            var agencies = [];
            var agencyAmounts = [];

            var types = [];
            var typeAmounts = [];

            for (var i = 0; i < arr.length; i++) {
                $('#body').append("<tr><td>" + arr[i].contractNumber + "</td><td>" + arr[i].awarded + "</td><td>" + arr[i].date + "</td><td>" + arr[i].companyName + "</td><td>" + arr[i].agency + "</td><td>" + arr[i].amount + "</td><td>" + arr[i].location + "</td><td>" + arr[i].type + "</td></tr>");

                if (agencies.includes(arr[i].agency)) {
                    agencyAmounts[agencies.indexOf(arr[i].agency)]++;
                } else {
                    console.log(arr[i].agency);
                    agencies.push(arr[i].agency);
                    agencyAmounts[agencies.length - 1] = 1;

                }

                if (types.includes(arr[i].type)) {
                    typeAmounts[types.indexOf(arr[i].type)]++;
                } else {
                    console.log(arr[i].types);
                    types.push(arr[i].type);
                    typeAmounts[types.length - 1] = 1;

                }

                arr[i].amount = arr[i].amount.replace("$", "").replaceAll(",","");
                console.log(arr[i]);
            }

            console.log(agencyAmounts);

            dataTable = $('#tbl').DataTable({
                paging: true,
                searching: true,
                ordering: true,
            });

            if (agenciesChart) agenciesChart.destroy();
            if (amountChart) amountChart.destroy();
            if (typesChart) typesChart.destroy();

            agenciesChart = new Chart("agencies", {
               type: "pie",

               data: {
                   labels: agencies,
                   datasets: [
                       {
                           data: agencyAmounts
                       }
                   ],
               },
               options: {
                   plugins: {
                       title: {
                           display: true,
                           text: "Federal agencies that contracted this company"
                       }
                   }
               }
            });

            amountChart = new Chart("amounts", {
                type: "scatter",

                data: {
                    datasets: [
                        {
                           data: arr
                        }
                    ]
                },
                options: {
                    parsing: {
                        xAxisKey: "timestamp",
                        yAxisKey: "amount"
                    },
                    scales: {
                        x:
                            {type: 'time',
                            time: {
                                unit: 'day'
                            }},
                        y: {
                            ticks: {
                                callback: function(value, index, ticks) {
                                    return '$' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                }
                            }
                         }

                    },

                    plugins: {
                        title: {
                            display: true,
                            text: "Amount vs Date"
                        },
                        legend: {
                            display: false
                        }
                    }
                }
            });

            typesChart = new Chart("types", {
                type: "pie",

                data: {
                    labels: types,
                    datasets: [
                        {
                            data: typeAmounts
                        }
                    ],
                },
                options: {
                    plugins: {
                        title: {
                            display: true,
                            text: "Types of contracts by this company"
                        }
                    }
                }
            });
            $('#tbl').show();

        });
    });
</script>
</body>
</html>