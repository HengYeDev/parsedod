<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Defense Contract Data Analysis</title>
    <!-- Favicon-->
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="styles.css" rel="stylesheet" />
</head>
<body>
<div class="d-flex" id="wrapper">
    <!-- Sidebar-->
    <div class="border-end bg-white" id="sidebar-wrapper">
        <div class="sidebar-heading border-bottom bg-light"><a href="/portal.jsp"><img src="logo.png" style="width: 200px"></a></div>
        <div class="list-group list-group-flush">
            <a class="list-group-item list-group-item-action list-group-item-light p-3" href="acquire.jsp">Acquire Data</a>
            <a class="list-group-item list-group-item-action list-group-item-light p-3" href="byCompany.jsp">By Company</a>
            <a class="list-group-item list-group-item-action list-group-item-light p-3" href="byAgency.jsp">By Agency</a>
            <a class="list-group-item list-group-item-action list-group-item-light p-3" href="byDate.jsp">By Date</a>

            <a class="list-group-item list-group-item-action list-group-item-light p-3" href="allData.jsp">All Data</a>
        </div>
    </div>
    <!-- Page content wrapper-->
    <div id="page-content-wrapper" class="h-100 w-100">
        <!-- Top navigation-->
        <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
            <div class="container-fluid">

            </div>
        </nav>
        <!-- Page content-->
        <div class="container-fluid h-100 w-100 mt-5">
            <div class="d-flex align-items-center justify-content-center h-100 w-100 mt-5">
                <div class="d-flex flex-column text-center">
                    <h1 class="text align-self-center p-2">Welcome</h1>
                    <h4 class="text align-self-center p-2">Use "Acquire Data" to retrieve and parse data for analysis and visualization.</h4>

                    <div class="mt-5 pt-5">
                        <h5 class="fw-normal">What can ParseDoD do?</h5>

                        <div class="row m-5" style="margin-left: 10em !important">
                            <div class="col-3 border-2 border p-5 bg-light rounded-2">
                                <p><strong class="fs-4">Parse</strong><br> Defense Contract Notices automatically from the DoD website using AI technology</p>
                            </div>
                            <div class="col-3 border-2 border p-5 offset-1 bg-light rounded-2">
                                <p><strong class="fs-4">Analyze</strong><br> company-specific trends and contract awards</p>
                            </div>
                            <div class="col-3 border-2 border p-5 offset-1 bg-light rounded-2">
                                <p><strong class="fs-4">Sort</strong><br> data from all Defense Contract Notices by price, agency, and company.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Bootstrap core JS-->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
<!-- Core theme JS-->
</body>
</html>