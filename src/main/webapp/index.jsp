<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="refresh" content="0; url=portal.jsp">
    <title>Redirecting...</title>
</head>
<body>
<p>If you are not redirected, <a href="portal.jsp">click here</a>.</p>
</body>
</html>
