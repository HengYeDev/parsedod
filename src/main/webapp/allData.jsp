<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>All Data</title>
    <!-- Favicon-->
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="styles.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/1.13.6/css/jquery.dataTables.min.css" rel="stylesheet">
</head>
<body>
<div class="d-flex" id="wrapper">
    <!-- Sidebar-->
    <div class="border-end bg-white" id="sidebar-wrapper">
        <div class="sidebar-heading border-bottom bg-light"><a href="/portal.jsp"><img src="logo.png" style="width: 200px"></a></div>
        <div class="list-group list-group-flush">
            <a class="list-group-item list-group-item-action list-group-item-light p-3" href="acquire.jsp">Acquire Data</a>
            <a class="list-group-item list-group-item-action list-group-item-light p-3
" href="byCompany.jsp">By Company</a>
            <a class="list-group-item list-group-item-action list-group-item-light p-3" href="byAgency.jsp">By Agency</a>
            <a class="list-group-item list-group-item-action list-group-item-light p-3" href="byDate.jsp">By Date</a>

            <a class="list-group-item list-group-item-action list-group-item-light p-3  bg-primary text-white" href="#">All Data</a>
        </div>
    </div>
    <!-- Page content wrapper-->
    <div id="page-content-wrapper" class="h-100 w-100">
        <!-- Top navigation-->
        <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom p-3">
            <div class="container-fluid">
                <h1 class="h3">All Data</h1>
            </div>
        </nav>
        <!-- Page content-->
        <div class="container-fluid h-100 w-100 mt-3">
            <button type="button" class="w-25 btn btn-primary d-inline-block" id="go">Retrieve All Data</button>
            <hr>
            <p class="insights"><a id="dl-csv" href="javascript:void(0);">Download CSV</a></p>
            <p id="status">All data will be loaded without filtering. This may take a while.</p>

            <table id="tbl" class="table table-striped">
                <thead>
                    <tr>
                        <th>Contract Number</th>
                        <th>Date Awarded</th>
                        <th>Expected Completion</th>
                        <th>Company Name</th>
                        <th>Federal Agency</th>
                        <th>Amount</th>
                        <th>Location</th>
                        <th>Type</th>
                    </tr>
                </thead>
                <tbody id="body">

                </tbody>
            </table>
            <hr>
            <h5 class="d-none insights">Locations of contracted companies</h5>
            <div id="map" style="height:750px; width: 800px"></div>
        </div>
    </div>
</div>
<table class="d-none" id="tbl2">
    <thead>
    <tr>
        <th>Contract Number</th>
        <th>Date Awarded</th>
        <th>Expected Completion</th>
        <th>Company Name</th>
        <th>Federal Agency</th>
        <th>Amount</th>
        <th>Location</th>
        <th>Type</th>
    </tr>
    </thead>
    <tbody id="body2">

    </tbody>
</table>
<!-- Bootstrap core JS-->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script src='https://cdn.datatables.net/1.13.5/js/jquery.dataTables.min.js'></script>
<script src='https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js'></script>
<script src='https://cdn.datatables.net/1.13.5/js/dataTables.bootstrap5.min.js'></script>
<script async
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBvDrrN1UnlblhVByHd4bJsBrDbejCR4wc&loading=async&callback=initMap">
</script>

<script src="table-to-csv.min.js"></script>
<!-- Core theme JS-->
<script>
    var tableHtml;
    $('#dl-csv').on('click', function() {
        new TableToCSV("#tbl2", {
            filename: 'report.csv',
            delimiter: ';'
        }).download();
    });
    $('#tbl').hide();
    $('.insights').hide();

    var dataTable;
    var geocoder;

    var map;
    var markers = [];

    function initMap() {
        var latlng = new google.maps.LatLng(37.09024, -95.712891);
        var myOptions = {
            zoom: 4,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        map = new google.maps.Map(document.getElementById("map"), myOptions);
        geocoder = new google.maps.Geocoder();
    }

    function codeAddress(address, popup) {
        try {
            geocoder.geocode({'address': address}, function (results, status) {
                if (status === 'OK') {
                    var marker = new google.maps.Marker({
                        map: map,
                        position: results[0].geometry.location
                    });


                    marker.addListener("click", function () {
                        const infoWindow = new google.maps.InfoWindow({
                            content: popup,
                            ariaLabel: popup
                        });

                        infoWindow.open({
                            anchor: marker,
                            map,
                        })
                    });
                    markers.push(marker);
                }
            });
        } catch(e) {
            console.log(e);
        }
    }

    function clearMarkers() {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(null);
        }
    }

    $('#map').hide();



    $('#go').click(function() {
        $('#tbl').hide();
        $('#status').text("Loading results...this may take awhile!");
        clearMarkers();

        if (dataTable)
            dataTable.destroy();
        $.get("/all-data", function(data) {
            var arr = data.records;
            $('.insights').removeClass('d-none');
            $('.insights').show();
            $('#map').show();
            $('#body').html("");
            $('#status').text(arr.length + " contracts found.");

            for (var i = 0; i < arr.length; i++) {
                $('#body').append("<tr><td>" + arr[i].contractNumber + "</td><td>" + arr[i].awarded + "</td><td>" + arr[i].date + "</td><td>" + arr[i].companyName + "</td><td>" + arr[i].agency + "</td><td>" + arr[i].amount + "</td><td>" + arr[i].location + "</td><td>" + arr[i].type + "</td></tr>");
                codeAddress(arr[i].location, arr[i].companyName);
                $('#body2').append("<tr><td>" + arr[i].contractNumber + "</td><td>" + arr[i].awarded + "</td><td>" + arr[i].date + "</td><td>" + arr[i].companyName + "</td><td>" + arr[i].agency + "</td><td>" + arr[i].amount + "</td><td>" + arr[i].location + "</td><td>" + arr[i].type + "</td></tr>");

            }

            dataTable = $('#tbl').DataTable({
                paging: true,
                searching: true,
                ordering: true
            });

            $('#tbl').show();

        });
    });


</script>
</body>
</html>