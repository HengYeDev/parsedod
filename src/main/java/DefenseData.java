import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import net.bytebuddy.build.Plugin;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import javax.servlet.ServletRequest;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DefenseData {
    private ArrayList<ContractDigest> digests;
    private ServletRequest request;
    private ArrayList<String> contracts;

    public DefenseData(ServletRequest request) {
        digests = new ArrayList<>();
        contracts = new ArrayList<>();
        this.request = request;
    }

    public void getDigests(Date start, Date end) {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--headless=new", "--disable-gpu");
        ChromeDriver driver = new ChromeDriver(options);

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String urlBase = "https://www.defense.gov/News/Contracts/StartDate/" + formatter.format(start) + "/EndDate/" + formatter.format(end) + "/";

        SimpleDateFormat parser = new SimpleDateFormat("MMM d, yyyy");
        int pageNumber = 1;
        boolean lastPage = false;
        do {
            driver.get(urlBase + "?page=" + pageNumber);
            if (!driver.findElements(By.className("no-items")).isEmpty()) {
                lastPage = true;
            }

            List<WebElement> links = driver.findElements(By.partialLinkText("Contracts For"));

            try (MongoClient mongoClient = MongoClients.create(request.getServletContext().getInitParameter("mongo.connection.str"))) {
                MongoDatabase db = mongoClient.getDatabase("defense");
                System.out.println("going");
                for (WebElement link : links) {
                    System.out.println(link.getText() + ":" + link.getAttribute("href"));

                    try {
                        digests.add(new ContractDigest(parser.parse(link.getText().replace("Contracts For ", "").replace(".", "")), link.getAttribute("href")));
                        parseArticle(link.getAttribute("href"), link.getText().replace("Contracts For ", ""), db);
                    } catch (ParseException | IOException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
            pageNumber++;
        } while (!lastPage);

        //save();
        System.out.println(digests);
    }

    public void parseArticle(String link, String date, MongoDatabase db) throws IOException, ParseException {
        SimpleDateFormat format = new SimpleDateFormat("MMM d, yyyy");
        try (CloseableHttpClient client = HttpClients.createDefault()) {
            HttpGet get = new HttpGet(link);

            try (CloseableHttpResponse response = client.execute(get)) {
                String html = IOUtils.toString(response.getEntity().getContent(), StandardCharsets.UTF_8);
                Document document = Jsoup.parse(html);

                Element element = document.selectFirst(".inside .body");

                assert element != null;

                String branchName = "";
                for (Element e : element.children()) {
                    if (e.wholeText().startsWith("*") || e.wholeText().isBlank() || e.html().contains("CORRECTION")) continue;
                    if (e.html().contains("<strong>")) {
                        branchName = e.wholeText();
                    } else {
                        try {
                            String analysis = contractToJson(e.wholeText());
                            JSONObject analysisObj = new JSONObject(analysis);
                            analysisObj.put("agency", branchName);
                            analysisObj.put("awarded", date);
                            analysisObj.put("type", analysisObj.getString("type").toLowerCase());

                            analysisObj.put("timestamp", format.parse(date.replace(".", "")).getTime());
                            contracts.add(analysisObj.toString());

                            db.getCollection("contracts").insertOne(org.bson.Document.parse(analysisObj.toString()));
                        } catch (Exception ea) {
                            System.out.println("parse error" + ea);
                        }

                    }
                }

            }
        }
    }

    public String contractToJson(String text) throws IOException {
        HttpClient client = HttpClients.createDefault();
        HttpPost post1 = new HttpPost("https://api.openai.com/v1/chat/completions");
        post1.setHeader("Content-Type", "application/json");
        post1.setHeader("Authorization", "Bearer " + request.getServletContext().getInitParameter("com.openai.key"));

        JSONArray messages = new JSONArray().put(new JSONObject()
                .put("role", "system")
                .put("content", "Read the paragraph and output ONLY a JSON object WITHOUT MARKDOWN FORMATTING with the keys 'amount', 'date', 'companyName', 'location', 'type', 'contractNumber' and ONLY string values, with ABSOLUTELY NO nested objects or arrays. The 'companyName' must only include the base company name with no location or branch name. The Amount should have a dollar sign and be comma separated. If there is more than one location, use the first one only. The 'type' should consist of the type of government contract from the document, such as 'fixed price' or 'time and materials', with no symbols or dashes. Note that 'modification' should not be used as a type. If you are unable to find the type, use 'Not included' in your JSON response. Never include duplicate keys. If there is more than one company listed, only use the first one. Give the simplest name of each company. Remove designations such as Inc., Corp., etc. If you can't find the contract number, put 'not included'"))
                .put(new JSONObject()
                        .put("role", "user")
                        .put("content", text));

        JSONObject obj = new JSONObject().put("temperature", 0.7).put("model", "gpt-3.5-turbo-0125").put("messages", messages);
        post1.setEntity(new StringEntity(obj.toString(), StandardCharsets.UTF_8));

        HttpResponse response1 = client.execute(post1);
        HttpEntity entity = response1.getEntity();

        String str = EntityUtils.toString(entity, StandardCharsets.UTF_8);

        return new JSONObject(str).getJSONArray("choices").getJSONObject(0).getJSONObject("message").getString("content");
    }

    public void save() {
        try (MongoClient mongoClient = MongoClients.create(request.getServletContext().getInitParameter("mongo.connection.str"))) {
            MongoDatabase db = mongoClient.getDatabase("defense");
            for (String s : contracts) {
                db.getCollection("contracts").insertOne(org.bson.Document.parse(s));
            }
        }
    }
}
