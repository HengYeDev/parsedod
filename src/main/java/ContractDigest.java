import java.util.Date;

public class ContractDigest {
    private Date date;
    private String link;

    public ContractDigest(Date date, String link) {
        this.date = date;
        this.link = link;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "contract{" +
                "date=" + date +
                ", link=" + link +
                '}';
    }
}
