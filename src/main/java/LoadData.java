import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

@WebServlet(name = "LoadData", value = "/load")
public class LoadData extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String dateRange = request.getParameter("date").replace(" ", "");
        String[] dates = dateRange.split("-");
        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");


        DefenseData data = new DefenseData(request);
        try {
            Date startDate = format.parse(dates[0]);
            Date endDate = format.parse(dates[1]);
            data.getDigests(startDate, endDate);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }

    }
}