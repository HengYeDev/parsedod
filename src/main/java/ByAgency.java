import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import org.bson.Document;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.regex.Pattern;

@WebServlet(name = "ByAgency", value = "/by-agency")
public class ByAgency extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String searchTerm = request.getParameter("term");
        try (MongoClient mongoClient = MongoClients.create(request.getServletContext().getInitParameter("mongo.connection.str"))) {
            MongoDatabase db = mongoClient.getDatabase("defense");
            String patternStr = ".*" + searchTerm + ".*";
            Pattern pattern = Pattern.compile(patternStr, Pattern.CASE_INSENSITIVE);
            JSONArray arr = new JSONArray();
            for (Document d : db.getCollection("contracts").find(Filters.regex("agency", pattern))) {
                arr.put(new JSONObject(d.toJson()));
            }
            response.setContentType("application/json");
            response.getWriter().print(new JSONObject().put("records", arr));
            response.getWriter().close();
        }
    }
}