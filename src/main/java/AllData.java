import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import org.bson.Document;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.regex.Pattern;

@WebServlet(name = "AllData", value = "/all-data")
public class AllData extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try (MongoClient mongoClient = MongoClients.create(request.getServletContext().getInitParameter("mongo.connection.str"))) {
            MongoDatabase db = mongoClient.getDatabase("defense");
            JSONArray arr = new JSONArray();

            for (Document d : db.getCollection("contracts").find()) {
                arr.put(new JSONObject(d.toJson()));
            }
            response.setContentType("application/json");
            response.getWriter().print(new JSONObject().put("records", arr));
            response.getWriter().close();
        }
    }
}