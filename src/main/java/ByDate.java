import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import org.bson.Document;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

@WebServlet(name = "Bydate", value = "/by-date")
public class ByDate extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String searchTerm = request.getParameter("term").replace(" ", "");
        String[] dates = searchTerm.split("-");
        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");


        try (MongoClient mongoClient = MongoClients.create(request.getServletContext().getInitParameter("mongo.connection.str"))) {
            MongoDatabase db = mongoClient.getDatabase("defense");
            Date startDate = format.parse(dates[0]);
            Date endDate = format.parse(dates[1]);
            JSONArray arr = new JSONArray();
            for (Document d : db.getCollection("contracts").find(new Document("timestamp", new Document("$gte", startDate.getTime()).append("$lte", endDate.getTime())))) {
                arr.put(new JSONObject(d.toJson()));
            }
            response.setContentType("application/json");
            response.getWriter().print(new JSONObject().put("records", arr));
            response.getWriter().close();
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }
}